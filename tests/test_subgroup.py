# Versão nova com 100% de cobertura
import pytest

from utils.subgroup_util import Subgroup

@pytest.mark.django_db
class TestSubgroupUtil:
    def setup_method(self):
        self.supply_voltage_negative = -1
        self.supply_voltage_70 = 70
        self.supply_voltage_69 = 69
        self.supply_voltage_0 = 0

    def test_get_subgroup_throws_exception_for_negative_voltage(self):
        with pytest.raises(Exception) as e:
            Subgroup.get_subgroup(self.supply_voltage_negative)
        assert 'Subgroup not found' in str(e.value)

    def test_get_subgroup_throws_exception_for_voltage_70(self):
        with pytest.raises(Exception) as e:
            Subgroup.get_subgroup(self.supply_voltage_70)
        assert 'Subgroup not found' in str(e.value)

    def test_get_subgroup_is_A3(self):
        assert Subgroup.get_subgroup(self.supply_voltage_69) == Subgroup.A3

    def test_get_subgroup_is_AS(self):
        assert Subgroup.get_subgroup(self.supply_voltage_0) == Subgroup.AS

    def test_get_all_subgroups(self):
        expected_subgroups = [
            {"name": Subgroup.AS, "min": 0, "max": 2.3},
            {"name": Subgroup.A4, "min": 2.3, "max": 25},
            {"name": Subgroup.A3A, "min": 30, "max": 44},
            {"name": Subgroup.A3, "min": 69, "max": 69},
            {"name": Subgroup.A2, "min": 88, "max": 138},
            {"name": Subgroup.A1, "min": 230, "max": None},
        ]
        assert Subgroup.get_all_subgroups() == expected_subgroups

# Versão antica com 96% de cobertura

# import pytest

# ENDPOINT = '/api/contracts/'

# from utils.subgroup_util import Subgroup

# @pytest.mark.django_db
# class TestContractEndpoint:
#     def setup_method(self):
#         self.contract_test_supply_voltage_1 = 250
#         self.contract_test_supply_voltage_2 = 100
#         self.contract_test_supply_voltage_3 = 40
#         self.contract_test_supply_voltage_4 = 70

#     def test_get_what_subgroup_contract_is_A1(self):
#         assert Subgroup.get_subgroup(self.contract_test_supply_voltage_1) == Subgroup.A1

#     def test_get_what_subgroup_contract_is_A2(self):
#         assert Subgroup.get_subgroup(self.contract_test_supply_voltage_2) == Subgroup.A2

#     def test_get_what_subgroup_contract_is_A3a(self):
#         assert Subgroup.get_subgroup(self.contract_test_supply_voltage_3) == Subgroup.A3A

#     def test_throws_exception_when_suply_voltage_does_not_match_ranges(self):
#         with pytest.raises(Exception) as e:
#             Subgroup.get_subgroup(self.contract_test_supply_voltage_4)

#         assert 'Subgroup not found' in str(e.value)